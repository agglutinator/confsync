#!/usr/bin/env python3
import argparse
import urllib
import logging
import os
import re
import requests
import settings as sts
import textwrap

from atlassian import Confluence
from bs4 import BeautifulSoup
from logging.config import dictConfig
from pathlib import Path

ENV_SOURCE_USERNAME = "CONF_SRC_USERNAME"
ENV_SOURCE_PASSWORD = "CONF_SRC_PASSWORD"
ENV_TARGET_USERNAME = "CONF_TGT_USERNAME"
ENV_TARGET_PASSWORD = "CONF_TGT_PASSWORD"

log_folder = Path(sts.LOG_FILE).parent
if not log_folder.exists():
    log_folder.mkdir(parents=True, exist_ok=True)

dictConfig(sts.LOGGING_CONFIG)


def get_creds():
    error = False
    source_username, source_password, target_username, target_password = [None] * 4
    if ENV_SOURCE_USERNAME in os.environ:
        source_username = os.environ[ENV_SOURCE_USERNAME]
    else:
        logging.error(f"{ENV_SOURCE_USERNAME} variable is not set")
        error = True
    if ENV_SOURCE_PASSWORD in os.environ:
        source_password = os.environ[ENV_SOURCE_PASSWORD]
    else:
        logging.error(f"{ENV_SOURCE_PASSWORD} variable is not set")
        error = True

    if ENV_TARGET_USERNAME in os.environ:
        target_username = os.environ[ENV_TARGET_USERNAME]
    else:
        logging.error(f"{ENV_TARGET_USERNAME} variable is not set")
        error = True
    if ENV_TARGET_PASSWORD in os.environ:
        target_password = os.environ[ENV_TARGET_PASSWORD]
    else:
        logging.error(f"{ENV_TARGET_PASSWORD} variable is not set")
        error = True

    if error:
        exit(1)
    return source_username, source_password, target_username, target_password


def get_page_id(url: str):
    match = re.search("pageId=(\d+)", url)
    if match:
        id = match.group(1)
        return int(id)
    logging.error(f"cannot determine page id from URL: {url}")
    return None


def get_server_url(url):
    parsed_url = urllib.parse.urlparse(url)
    scheme = parsed_url.scheme
    host = parsed_url.hostname
    parsed_url = f"{scheme}://{host}"
    return parsed_url


def main(input_params: argparse.Namespace):
    source_username, source_password, target_username, target_password = get_creds()

    source_page = input_params.inputpage
    target_page = input_params.outputpage

    server_url = get_server_url(source_page)
    source_confluence = Confluence(
        url=server_url,
        username=source_username,
        password=source_password)
    logging.debug("got source confluence endpoint")

    server_url = get_server_url(target_page)
    target_confluence = Confluence(
        url=server_url,
        username=target_username,
        password=target_password)
    logging.debug("got target confluence endpoint")

    # load a page
    source_page_id = get_page_id(source_page)
    target_page_id = get_page_id(target_page)
    page = source_confluence.get_page_by_id(source_page_id, expand="space,body.view,container")
    logging.debug(f"page {source_page} exists={page}")
    title = page["title"]
    body = page["body"]["view"]["value"]
    soup = BeautifulSoup(body, 'html.parser')
    target_confluence.update_page(
        target_page_id,
        title,
        str(soup),
        parent_id=None,
        type='page',
        representation='storage',
        minor_edit=False,
        full_width=False
    )

    # load attachments
    attachments_container = source_confluence.get_attachments_from_content(page_id=source_page_id, start=0, limit=100)
    attachments = attachments_container['results']
    for attachment in attachments:
        title = attachment['title']
        comment = attachment["metadata"]["comment"]
        media_type = attachment["metadata"]["mediaType"]
        labels = attachment["metadata"]["labels"]["results"]
        download_link = source_confluence.url + attachment['_links']['download']
        r = requests.get(download_link, auth=(source_username, source_password))
        target_confluence.attach_content(
            r.content,
            name=title,
            content_type=media_type,
            page_id=str(target_page_id),
            title=title,
            comment=comment
        )
        if len(labels) > 0:
            # TODO: set labels to attachment
            pass


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='confsync',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=textwrap.dedent('''\
                Before running the tool, please set the environment variables:
                CONF_SRC_USERNAME - source Confluence user name 
                CONF_SRC_PASSWORD - source Confluence password
                CONF_TGT_USERNAME - target Confluence user name
                CONF_TGT_PASSWORD - target Confluence password
         '''))
    parser.add_argument('-ip',
                        '--inputpage',
                        type=str,
                        required=False,
                        help='URL to page to sync from'
                        )
    parser.add_argument('-op',
                        '--outputpage',
                        type=str,
                        required=False,
                        help='URL to page to sync to',
                        )

    input_params = parser.parse_args()
    main(input_params)

