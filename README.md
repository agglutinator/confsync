# Confluence Sync #

A script to synchronize one page from one Confluence instance to another.

## Installation ##

- Create Python virtual environment
- run `pip install -r requirements.txt`

## Command line parameters ##

    usage: confsync [-h] [-ip INPUTPAGE] [-op OUTPUTPAGE]
  
    options:
      -h, --help            show this help message and exit
      -ip INPUTPAGE, --inputpage INPUTPAGE
                            URL to page to sync from
      -op OUTPUTPAGE, --outputpage OUTPUTPAGE
                            URL to page to sync to
  
    Before running the tool, please set the environment variables:
    CONF_SRC_USERNAME - source Confluence user name 
    CONF_SRC_PASSWORD - source Confluence password
    CONF_TGT_USERNAME - target Confluence user name
    CONF_TGT_PASSWORD - target Confluence password

## TODO ##
- Copy labels for pages and attachments.
